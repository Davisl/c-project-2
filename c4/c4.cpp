// 9/13/2019
//CIT 245 Data Structures and Algorithms C++
//Leland Davis


#include <iostream> 
using namespace std;

int gcd(int num1, int num2)
{
	if (num2 == 0)
		return num1;
	return gcd(num2, num1 % num2);

}


int main()
{
	int num1, num2;
	int den, a, b;
	int choice;

	do {
		cout << "This program takes a numerator and denominator and reduces to lowest terms\n"
			<< "\n"
			<< "Enter the numerator\n";
		cin >> num1;
		cout << "Enter the denominator\n";
		cin >> num2;

		den = gcd(num1, num2);
		a = num1 / den;
		b = num2 / den;

		cout << "greatest common denomonator is " << gcd(num1, num2) << "\n"
			<< "Your fraction reduced is " << a << "/" << b << "\n";
		cout << "Try Again? (1 = yes, 0 = exit)";
		cin >> choice;

		
	} while (choice > 0);
	system("pause");
}